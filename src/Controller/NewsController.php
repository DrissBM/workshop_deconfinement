<?php

namespace App\Controller;

use App\Entity\Interest;
use App\Entity\News;
use App\Entity\NewsAction;
use App\Entity\User;
use App\Form\NewsType;
use App\Repository\EventsActionRepository;
use App\Repository\InterestRepository;
use App\Repository\NewsActionRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news")
 */
class NewsController extends AbstractController
{
    private $newsActionRepository;
    private $interestRepository;

    public function __construct(NewsActionRepository $newsActionRepository, InterestRepository $interestRepository)
    {
        $this->newsActionRepository = $newsActionRepository;
        $this->interestRepository = $interestRepository;
    }
    /**
     * @Route("/", name="news_index", methods={"GET"})
     */
    public function index(NewsRepository $newsRepository): Response
    {
        $news = $newsRepository->findByDate();
        $filters = [];
        foreach ($news as $new) {
            array_push($filters, $new->getTag()->getName());
        }
        $filters = array_unique($filters);
        return $this->render('news/index.html.twig', [
            'news' => $news,
            'filters' => $filters,
        ]);
    }

    /**
     * @Route("/new", name="news_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_show", methods={"GET","POST"})
     */
    public function show(News $news, EventsActionRepository $eventActionRepo): Response
    {
        $user = $this->getUser();
        $interest = $news->getTag();
        $type = $this->newsActionRepository->findByNewsNameAndUser($news, $user);
        $events = $eventActionRepo->findBy(
            [
                'User' => $user,
                'isAccepted' => true
            ]
        );

        if (!$type) {
            $entityManager = $this->getDoctrine()->getManager();
            $typei = $this->interestRepository->findByTagAndUser($interest, $user);
            if (!$typei) {
                $typei = new Interest();
                $typei
                    ->setUser($user)
                    ->setCategory($news->getTag())
                    ->setXp(5);
                $entityManager->persist($typei);
            } else {
                $typei->setXP($typei->getXP() + 5);
                $entityManager->persist($typei);
            }
            $type = new NewsAction();
            $type
                ->setUser($user)
                ->setNews($news);
            $entityManager->persist($type);
            $user->setXP($user->getXP() + 5);
            $entityManager->persist($user);
            $entityManager->flush();
        }


        return $this->render('news/show.html.twig', [
            'news' => $news,
            'eventsAccepted' => $events,
            'type' => $type
        ]);
    }

    /**
     * @Route("/{id}/like", name="news_like", methods={"GET","POST"})
     */
    public function like(News $news): Response
    {
        $added = false;
        $user = $this->getUser();
        $interest = $news->getTag();
        $liked = $this->newsActionRepository->findByNewsNameAndUser($news, $user);
        if ($liked->getOpinion() == 0) {
            $liked->setOpinion(1);
            $typei = $this->interestRepository->findByTagAndUser($interest, $user);
            $typei->setXP($typei->getXP() + 5);
            $user->setXP($user->getXP() + 5);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typei);
            $entityManager->persist($liked);
            $entityManager->persist($user);
            $entityManager->flush();
            $added = true;
        }

        return $this->json(["result" => true, "added" => $added]);
    }
    /**
     * @Route("/{id}/dislike", name="news_dislike", methods={"GET","POST"})
     */
    public function dislike(News $news): Response
    {
        $added = false;
        $user = $this->getUser();
        $interest = $news->getTag();
        $liked = $this->newsActionRepository->findByNewsNameAndUser($news, $user);
        if ($liked->getOpinion() == 0) {
            $liked->setOpinion(2);
            $typei = $this->interestRepository->findByTagAndUser($interest, $user);
            $typei->setXP($typei->getXP() - 5);
            $user->setXP($user->getXP() + 5);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typei);
            $entityManager->persist($liked);
            $entityManager->persist($user);
            $entityManager->flush();
            $added = true;
        }
        return $this->json(["result" => true, "added" => $added]);
    }

    /**
     * @Route("/{id}/edit", name="news_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, News $news): Response
    {
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_delete", methods={"DELETE"})
     */
    public function delete(Request $request, News $news): Response
    {
        if ($this->isCsrfTokenValid('delete' . $news->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($news);
            $entityManager->flush();
        }

        return $this->redirectToRoute('news_index');
    }
}
