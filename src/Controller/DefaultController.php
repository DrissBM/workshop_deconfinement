<?php

namespace App\Controller;

use App\Entity\Events;
use App\Entity\EventsAction;
use App\Entity\Interest;
use App\Form\UserType;
use App\Repository\EventsActionRepository;
use App\Repository\EventsRepository;
use App\Repository\InterestRepository;
use App\Repository\NewsActionRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */

    public function index()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home_connected');
        }

        return $this->redirectToRoute('app_welcome');
    }

    /**
     * @Route("/home", name="home_connected")
     */
    public function home(EventsRepository $eventsRepository, EventsActionRepository $eventsactionRepo, NewsRepository $newsRepository, NewsActionRepository $newsactionRepo, InterestRepository $interestRepo)
    {
        // Gestion events
        $events = $eventsRepository->findAll();
        $user = $this->getUser();
        $userAccepted = [];
        $userEvents = [];
        foreach ($events as $event) {
            $eventsDoneBy = $eventsactionRepo->findByEventAndUser($event, $user);
            if (!$eventsDoneBy) {
                array_push($userEvents, $event);
            } else {
                foreach ($eventsDoneBy as $eventDone) {
                    if ($eventDone->getUser() == $user && $eventDone->getIsAccepted()) {
                        array_push($userAccepted, $eventDone);
                    }
                }
            }
        }
        // Gestion news
        $filters = [];
        $userinterests = $interestRepo->findFilterByUser($user);
        foreach ($userinterests as $inter) {
            array_push($filters, $inter->getCategory());
        }
        $news = $newsRepository->findNewsWithFilters($filters);
        $mynews = [];
        foreach ($news as $new) {
            $newsread = $newsactionRepo->findByNewsNameAndUser($new, $user);
            if (!$newsread) {
                array_push($mynews, $new);
            }
        }
        return $this->render('default/index.html.twig', [
            'events' => $userEvents,
            'eventsAccepted' => $userAccepted,
            'news' => $mynews,
        ]);
    }

    /**
     * @Route("/settings", name="settings_user")
     */
    public function settings(Request $request): Response
    {
        $user = $this->getUser();

        return $this->render('user/setting.html.twig', [
            'user' => $user,
        ]);
    }
    /**
     * @Route("/confi", name="confi")
     */
    public function confi(Request $request): Response
    {
        return $this->render('default/confi.html.twig');
    }
    /**
     * @Route("/condi", name="condi")
     */
    public function condi(Request $request): Response
    {
        return $this->render('default/condi.html.twig');
    }
    /**
     * @Route("/profil", name="profil_user")
     */
    public function profil(Request $request, InterestRepository $interestRepo, EventsActionRepository $eventActionRepo): Response
    {
        $user = $this->getUser();
        $events = $eventActionRepo->findBy(
            [
                'User' => $user,
                'isAccepted' => true
            ]
        );
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $interets = $interestRepo->findBy(
            ['user' => $user]
        );
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('user/index.html.twig', [
            'eventsAccepted' => $events,
            'interets' => $interets,
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/accept", name="event_accept", methods={"GET","POST"})
     */
    public function accept(Events $event, InterestRepository $interestRepo): Response
    {
        $user = $this->getUser();
        $eventAction = new EventsAction();
        $eventAction
            ->setUser($user)
            ->setEvent($event)
            ->setIsAccepted(true);
        $entityManager = $this->getDoctrine()->getManager();
        $interest = $event->getTag();
        $typei = $interestRepo->findByTagAndUser($interest, $user);
        if (!$typei) {
            $typei = new Interest();
            $typei
                ->setUser($user)
                ->setCategory($event->getTag())
                ->setXp(10);
            $entityManager->persist($typei);
        } else {
            $typei->setXP($typei->getXP() + 10);
            $entityManager->persist($typei);
        }

        $user->setXP($user->getXP() + 10);

        $entityManager->persist($eventAction);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('home_connected');
    }
    /**
     * @Route("/{id}/reject", name="event_reject", methods={"GET","POST"})
     */
    public function reject(Events $event): Response
    {
        $user = $this->getUser();
        $eventAction = new EventsAction();
        $eventAction
            ->setUser($user)
            ->setEvent($event)
            ->setIsAccepted(false);
        $user->setXP($user->getXP() + 10);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($eventAction);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->json(["result" => true]);
    }

    /**
     * @Route("/welcome", name="app_welcome", methods={"GET"})
     */
    public function choix(): Response
    {
        return $this->render('default/welcome.html.twig');
    }
}
