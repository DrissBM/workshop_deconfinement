<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('abstract')
            ->add('description')
            ->add('pays', ChoiceType::class, [
                'choices'  => [
                    'Suisse' => 'Suisse',
                    'France' => 'France',
                ],
            ])
            ->add('publishing_date')
            ->add('source')
            ->add('tag')
            ->add('link')
            ->add('srcimg');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
