<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    // /**
    //  * @return News[] Returns an array of News objects
    //  */
    public function findNewsWithFilters($value)
    {
        $filter1 = $value[0];
        $filter2 = '';
        $filter3 = '';
        $size = count($value);
        if ($size == 2) {
            $filter2 = $value[1];
        } elseif ($size == 3) {
            $filter2 = $value[1];
            $filter2 = $value[2];
        }

        return $this->createQueryBuilder('n')
            ->andWhere('n.tag = :filter1')
            ->setParameter('filter1', $filter1)
            ->orWhere('n.tag = :filter2')
            ->setParameter('filter2', $filter2)
            ->orWhere('n.tag = :filter3')
            ->setParameter('filter3', $filter3)
            ->getQuery()
            ->getResult();
    }
    public function findByDate()
    {

        return $this->createQueryBuilder('n')
            ->orderBy('n.publishing_date', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?News
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
