<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\NewsAction;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewsAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsAction[]    findAll()
 * @method NewsAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsAction::class);
    }

    // /**
    //  * @return NewsAction[] Returns an array of NewsAction objects
    //  */
    public function findByNewsNameAndUser(News $news, User $user)
    {
        return $this->createQueryBuilder('n')
            ->where('n.user = :user')
            ->setParameter('user', $user)
            ->andWhere('n.news = :val')
            ->setParameter('val', $news)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /*
    public function findOneBySomeField($value): ?NewsAction
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
