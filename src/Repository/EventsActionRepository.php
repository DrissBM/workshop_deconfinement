<?php

namespace App\Repository;

use App\Entity\Events;
use App\Entity\EventsAction;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventsAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventsAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventsAction[]    findAll()
 * @method EventsAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventsActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventsAction::class);
    }

    // /**
    //  * @return EventsAction[] Returns an array of EventsAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByEventAndUser(Events $value, User $user)
    {
        return $this->createQueryBuilder('e')
            ->where('e.User = :user')
            ->setParameter('user', $user)
            ->andWhere('e.Event = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }
}
