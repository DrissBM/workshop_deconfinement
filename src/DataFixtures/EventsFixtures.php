<?php

namespace App\DataFixtures;

use App\Entity\Events;
use App\Repository\CategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EventsFixtures extends Fixture implements DependentFixtureInterface
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $categories = $this->categoryRepository->findAll();
        $allevents = [
            ["EventsTitle", "EventsDescription", "Suisse", "festif"],
            ["EventsTitle2", "EventsDescription2", "Suisse", "education"],
        ];

        foreach ($allevents as $events) {
            $event = new Events();
            foreach ($categories as $category) {
                if ($category->getName() == $events[3]) {
                    $cat = $category;
                }
            }
            $event
                ->setTitle($events[0])
                ->setDescription($events[1])
                ->setPays($events[2])
                ->setTag($cat);

            $manager->persist($event);
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
