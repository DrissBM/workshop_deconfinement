<?php

namespace App\DataFixtures;

use App\Entity\Rules;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RulesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $allrules = [
            ["RulesTitle", "RulesAbstract", "RulesDescription", "12-10-1998", "Suisse", "12-10-2020"],
            ["RulesTitle2", "RulesAbstract2", "RulesDescription2", "12-10-1998", "Suisse", "12-10-2020"],
        ];
        foreach ($allrules as $rules) {
            $rule = new Rules();
            $rule
                ->setTitle($rules[0])
                ->setAbstract($rules[1])
                ->setDescription($rules[2])
                ->setPublishingDate(new \DateTime($rules[3]))
                ->setPays($rules[4])
                ->setEndDate(new \DateTime($rules[5]));

            $manager->persist($rule);
        }

        $manager->flush();
    }
}
