<?php

namespace App\DataFixtures;

use App\Entity\News;
use App\Repository\CategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $categories = $this->categoryRepository->findAll();
        $allnews = [
            ["NewsTitle", "NewsAbstract", "NewsDescription", "12-10-1998", "RTS", "Suisse", "festif"],
            ["NewsTitle2", "NewsAbstract2", "NewsDescription2", "12-10-1998", "RTS", "Suisse", "education"],
        ];

        foreach ($allnews as $news) {
            $new = new News();
            foreach ($categories as $category) {
                if ($category->getName() == $news[6]) {
                    $cat = $category;
                }
            }
            $new
                ->setTitle($news[0])
                ->setAbstract($news[1])
                ->setDescription($news[2])
                ->setPublishingDate(new \DateTime($news[3]))
                ->setSource($news[4])
                ->setPays($news[5])
                ->setTag($cat);

            $manager->persist($new);
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
