<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventsActionRepository")
 */
class EventsAction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAccepted;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Events", inversedBy="eventsActions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="eventsActions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    public function __toString()
    {
        return $this->getId() . "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(bool $isAccepted): self
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    public function getEvent(): ?Events
    {
        return $this->Event;
    }

    public function setEvent(?Events $Event): self
    {
        $this->Event = $Event;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }
}
