<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $abstract;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pays;

    /**
     * @ORM\Column(type="date")
     */
    private $publishing_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsAction", mappedBy="news", orphanRemoval=true)
     */
    private $newsActions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="news")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tag;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $srcimg;

    public function __construct()
    {
        $this->newsActions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAbstract(): ?string
    {
        return $this->abstract;
    }

    public function setAbstract(string $abstract): self
    {
        $this->abstract = $abstract;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getPublishingDate(): ?\DateTimeInterface
    {
        return $this->publishing_date;
    }

    public function setPublishingDate(\DateTimeInterface $publishing_date): self
    {
        $this->publishing_date = $publishing_date;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return Collection|NewsAction[]
     */
    public function getNewsActions(): Collection
    {
        return $this->newsActions;
    }

    public function addNewsAction(NewsAction $newsAction): self
    {
        if (!$this->newsActions->contains($newsAction)) {
            $this->newsActions[] = $newsAction;
            $newsAction->setNews($this);
        }

        return $this;
    }

    public function removeNewsAction(NewsAction $newsAction): self
    {
        if ($this->newsActions->contains($newsAction)) {
            $this->newsActions->removeElement($newsAction);
            // set the owning side to null (unless already changed)
            if ($newsAction->getNews() === $this) {
                $newsAction->setNews(null);
            }
        }

        return $this;
    }

    public function getTag(): ?Category
    {
        return $this->tag;
    }

    public function setTag(?Category $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getSrcimg(): ?string
    {
        return $this->srcimg;
    }

    public function setSrcimg(string $srcimg): self
    {
        $this->srcimg = $srcimg;

        return $this;
    }
}
