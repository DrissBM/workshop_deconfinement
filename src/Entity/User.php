<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Pays;

    /**
     * @ORM\Column(type="integer")
     */
    private $xp;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsAction", mappedBy="user", orphanRemoval=true)
     */
    private $newsActions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventsAction", mappedBy="User", orphanRemoval=true)
     */
    private $eventsActions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Interest", mappedBy="user", orphanRemoval=true)
     */
    private $interests;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $avatar_name;

    public function __construct()
    {
        $this->newsActions = new ArrayCollection();
        $this->eventsActions = new ArrayCollection();
        $this->interests = new ArrayCollection();
        $this->xp = 0;
        $this->avatar = '';
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPays(): ?string
    {
        return $this->Pays;
    }

    public function setPays(string $Pays): self
    {
        $this->Pays = $Pays;

        return $this;
    }

    public function getXp(): ?int
    {
        return $this->xp;
    }

    public function setXp(int $xp): self
    {
        $this->xp = $xp;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection|NewsAction[]
     */
    public function getNewsActions(): Collection
    {
        return $this->newsActions;
    }

    public function addNewsAction(NewsAction $newsAction): self
    {
        if (!$this->newsActions->contains($newsAction)) {
            $this->newsActions[] = $newsAction;
            $newsAction->setUser($this);
        }

        return $this;
    }

    public function removeNewsAction(NewsAction $newsAction): self
    {
        if ($this->newsActions->contains($newsAction)) {
            $this->newsActions->removeElement($newsAction);
            // set the owning side to null (unless already changed)
            if ($newsAction->getUser() === $this) {
                $newsAction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventsAction[]
     */
    public function getEventsActions(): Collection
    {
        return $this->eventsActions;
    }

    public function addEventsAction(EventsAction $eventsAction): self
    {
        if (!$this->eventsActions->contains($eventsAction)) {
            $this->eventsActions[] = $eventsAction;
            $eventsAction->setUser($this);
        }

        return $this;
    }

    public function removeEventsAction(EventsAction $eventsAction): self
    {
        if ($this->eventsActions->contains($eventsAction)) {
            $this->eventsActions->removeElement($eventsAction);
            // set the owning side to null (unless already changed)
            if ($eventsAction->getUser() === $this) {
                $eventsAction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Interest[]
     */
    public function getInterests(): Collection
    {
        return $this->interests;
    }

    public function addInterest(Interest $interest): self
    {
        if (!$this->interests->contains($interest)) {
            $this->interests[] = $interest;
            $interest->setUser($this);
        }

        return $this;
    }

    public function removeInterest(Interest $interest): self
    {
        if ($this->interests->contains($interest)) {
            $this->interests->removeElement($interest);
            // set the owning side to null (unless already changed)
            if ($interest->getUser() === $this) {
                $interest->setUser(null);
            }
        }

        return $this;
    }

    public function getAvatarName(): ?string
    {
        return $this->avatar_name;
    }

    public function setAvatarName(string $avatar_name): self
    {
        $this->avatar_name = $avatar_name;

        return $this;
    }
}
