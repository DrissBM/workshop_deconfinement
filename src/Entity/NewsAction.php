<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsActionRepository")
 */
class NewsAction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $opinion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\News", inversedBy="newsActions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="newsActions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->opinion = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOpinion(): ?int
    {
        return $this->opinion;
    }

    public function setOpinion(int $opinion): self
    {
        $this->opinion = $opinion;

        return $this;
    }

    public function getNews(): ?News
    {
        return $this->news;
    }

    public function setNews(?News $news): self
    {
        $this->news = $news;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
