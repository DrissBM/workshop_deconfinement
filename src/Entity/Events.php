<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventsRepository")
 */
class Events
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventsAction", mappedBy="Event")
     */
    private $eventsActions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tag;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pays;

    public function __construct()
    {
        $this->eventsActions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|EventsAction[]
     */
    public function getEventsActions(): Collection
    {
        return $this->eventsActions;
    }

    public function addEventsAction(EventsAction $eventsAction): self
    {
        if (!$this->eventsActions->contains($eventsAction)) {
            $this->eventsActions[] = $eventsAction;
            $eventsAction->setEvent($this);
        }

        return $this;
    }

    public function removeEventsAction(EventsAction $eventsAction): self
    {
        if ($this->eventsActions->contains($eventsAction)) {
            $this->eventsActions->removeElement($eventsAction);
            // set the owning side to null (unless already changed)
            if ($eventsAction->getEvent() === $this) {
                $eventsAction->setEvent(null);
            }
        }

        return $this;
    }

    public function getTag(): ?Category
    {
        return $this->tag;
    }

    public function setTag(?Category $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }
}
