<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425134147 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, tag_id INT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_5387574ABAD26311 (tag_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events_action (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, user_id INT NOT NULL, is_accepted TINYINT(1) NOT NULL, INDEX IDX_9571B5A771F7E88B (event_id), INDEX IDX_9571B5A7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interest (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, category_id INT NOT NULL, xp INT NOT NULL, INDEX IDX_6C3E1A67A76ED395 (user_id), INDEX IDX_6C3E1A6712469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_action (id INT AUTO_INCREMENT NOT NULL, news_id INT NOT NULL, user_id INT NOT NULL, opinion INT NOT NULL, INDEX IDX_CB654FE6B5A459A0 (news_id), INDEX IDX_CB654FE6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574ABAD26311 FOREIGN KEY (tag_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE events_action ADD CONSTRAINT FK_9571B5A771F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('ALTER TABLE events_action ADD CONSTRAINT FK_9571B5A7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE interest ADD CONSTRAINT FK_6C3E1A67A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE interest ADD CONSTRAINT FK_6C3E1A6712469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE news_action ADD CONSTRAINT FK_CB654FE6B5A459A0 FOREIGN KEY (news_id) REFERENCES news (id)');
        $this->addSql('ALTER TABLE news_action ADD CONSTRAINT FK_CB654FE6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE news ADD tag_id INT NOT NULL, ADD source VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950BAD26311 FOREIGN KEY (tag_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_1DD39950BAD26311 ON news (tag_id)');
        $this->addSql('ALTER TABLE user ADD pays VARCHAR(255) NOT NULL, ADD xp INT NOT NULL, ADD avatar VARCHAR(100) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events_action DROP FOREIGN KEY FK_9571B5A771F7E88B');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE events_action');
        $this->addSql('DROP TABLE interest');
        $this->addSql('DROP TABLE news_action');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950BAD26311');
        $this->addSql('DROP INDEX IDX_1DD39950BAD26311 ON news');
        $this->addSql('ALTER TABLE news DROP tag_id, DROP source');
        $this->addSql('ALTER TABLE user DROP pays, DROP xp, DROP avatar');
    }
}
