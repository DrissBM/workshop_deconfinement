# Symphony

Feel free to contribuate for improving this starter.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need of course node, composer, php installed on your computer

### Installing

Firstly, please run the command below for installing all dependencies.

```
composer install
```

## Developing

You can run the command below for starting the live server previewing devlopment environment.

```
symfony serve
```

## Authors

- **Driss Ben Mimoun**
